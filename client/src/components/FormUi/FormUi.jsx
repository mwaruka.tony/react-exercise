import React, { useState, Fragment } from "react";
import isEmpty from "lodash.isempty";
import stateData from "../../utils/states";
import ResultDisplay from "../ResultDisplay/ResultDisplay";
import { Form } from "./styles";

function FormUi() {
  const [selectedState, setSelectedState] = useState("");
  const [selectedRep, setSelectedRep] = useState("");
  const [options, setOptions] = useState({});

  function handleSubmit(event) {
    event.preventDefault();
    setOptions({ state: selectedState, type: selectedRep });
  }

  function handleChange(event) {
    const value = event.target.value,
      selectedType = event.target.name;

    selectedType === "stateName"
      ? setSelectedState(value)
      : setSelectedRep(value);
  }

  return (
    <Fragment>
      <Form onSubmit={handleSubmit}>
        <select
          name="repType"
          form="representationForm"
          onChange={handleChange}>
          <option value="">Select by Representative or by Senator</option>
          <option value="rep">Representative</option>
          <option value="sen">Senator</option>
        </select>

        <select
          name="stateName"
          form="representationForm"
          onChange={handleChange}>
          <option value="">Choose State</option>
          {stateData.map((values, i) => (
            <option key={i} value={values["value"]}>
              {values["label"]}
            </option>
          ))}
        </select>
        <button disabled={selectedState && selectedRep ? false : true}>
          Submit
        </button>
      </Form>
      {!isEmpty(options) && <ResultDisplay options={options} />}
    </Fragment>
  );
}

export default FormUi;
