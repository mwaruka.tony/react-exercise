import styled from "styled-components";

export const Form = styled.form`
  select {
    font-size: 14px;
    padding: 10px;
    font-family: "Lato", sans-serif;
    font-weight: 500;
    margin-right: 10px;
  }

  input {
    padding: 10px;
  }
  button {
    font-size: 17px;
    padding: 10px 20px;
    background: #03a6ed;
    color: #fff;
    outline: none;
    border: none;
    border-radius: 2px;
    cursor: pointer;
    transition: 0.25s;
    &:disabled {
      opacity: 0.45;
      cursor: initial;
    }
  }
`;
