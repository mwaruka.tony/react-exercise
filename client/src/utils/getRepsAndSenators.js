import { useEffect, useState } from "react";

export default function useRepData(options) {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState({});
  const path = options.type === "rep" ? "/representatives/" : "/senators/";
  const url = path + options.state.toLowerCase();

  useEffect(() => {
    setLoading(true);

    fetch(url, {
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((r) => r.json())
      .then((res) => {
        if (res) {
          setLoading(false);
          setData(res);
        }
      })
      .catch((error) => error);
  }, [url]);

  return { loading, data };
}
