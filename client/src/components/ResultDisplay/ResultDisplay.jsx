import React, { useState, Fragment } from "react";
import PropTypes from "prop-types";
import useRepData from "../../utils/getRepsAndSenators";
import { ListWrapper } from "./styles";

function ResultDisplay(props) {
  const [info, setInfo] = useState([]);
  const { options } = props;
  const { loading, data } = useRepData(options);

  function handleClick(event) {
    const currentInfo = data.results.filter(
      (person) => person.phone === event.currentTarget.dataset.phone
    );

    setInfo(currentInfo);
  }

  function getFirstAndLastName(name) {
    const [firstName, ...rest] = name.split(" ");
    return (
      <Fragment>
        <p>
          <span>First Name:</span> {firstName}
        </p>
        <p>
          <span>Last Name:</span> {rest.join(" ")}
        </p>
      </Fragment>
    );
  }

  return (
    <Fragment>
      <ListWrapper>
        <div className="names">
          <h2>
            List / <span>Representatives</span>
          </h2>
          <div className="row">
            <div>
              <span>Name</span>
              <span>Party</span>
            </div>
            {data &&
              data.results &&
              data.results.map((v, i) => (
                <div key={i} data-phone={`${v.phone}`} onClick={handleClick}>
                  <span>{v.name}</span>
                  <span>{v.party.charAt(0)}</span>
                </div>
              ))}
          </div>
        </div>
        <div className="info">
          <h2>Info</h2>
          <div>
            {info && info[0] && (
              <Fragment>
                {getFirstAndLastName(info[0].name)}
                {info[0].district && (
                  <p>
                    <span>District:</span> <span></span>
                    {info[0].district}
                  </p>
                )}
                {info[0].phone && (
                  <p>
                    <span>Phone:</span> {info[0].phone}
                  </p>
                )}
                {info[0].office && (
                  <p>
                    <span>Office: </span>
                    {info[0].office}
                  </p>
                )}
                {info[0].link && (
                  <p>
                    <span>Website:</span> {info[0].link}
                  </p>
                )}
              </Fragment>
            )}
          </div>
        </div>
      </ListWrapper>
      {loading && <div>LOADING...</div>}
    </Fragment>
  );
}

ResultDisplay.propTypes = {
  options: PropTypes.object.isRequired,
};

export default ResultDisplay;
