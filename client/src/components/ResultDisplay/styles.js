import styled from "styled-components";

export const ListWrapper = styled.div`
  display: flex;
  h2 {
    font-weight: 300;
    span {
      font-size: 24px;
      color: #03a6ed;
    }
  }
  .names {
    max-width: 380px;
    width: 100%;
    margin-right: 30px;
  }

  .row {
    div {
      display: flex;
      justify-content: space-between;
      padding: 15px 50px 15px 5px;
      border-bottom: 1px solid #eaeaea;
      &:first-of-type {
        background: #f8f8f9;
        border-bottom: none;
      }
    }
  }

  .info {
    max-width: 380px;
    width: 100%;
    p {
      background: #f4f4f5;
      margin-bottom: 10px;
      padding: 10px 5px;
      border-radius: 5px;
      font-weight: 400;
      opacity: 0.45;
      span {
        padding-right: 15px;
      }
    }
  }
`;
