import React from "react";
import FormUi from "../../components/FormUi/FormUi";
import { Wrapper } from "./styles";

function Dashboard(props) {
  return (
    <Wrapper className="mainWrapper">
      <h1>Who's My Representative?</h1>
      <FormUi />
    </Wrapper>
  );
}

export default Dashboard;
