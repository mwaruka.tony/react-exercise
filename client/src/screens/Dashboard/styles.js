import styled from "styled-components";

export const Wrapper = styled.div`
  h1 {
    color: #03a6ed;
    font-size: 21px;
    &:after {
      content: "";
      display: block;
      margin-top: 40px;
      border-bottom: 1px solid #eaeaea;
    }
  }
`;
