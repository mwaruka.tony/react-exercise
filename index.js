const express = require("express");
const request = require("request");
const cors = require("cors");
const path = require("path");

const app = express();
const PORT = process.env.PORT || 5000;
app.use(cors());

app.get("/representatives/:state", findRepresentativesByState, jsonResponse);

app.get("/senators/:state", findSenatorsByState, jsonResponse);

function findRepresentativesByState(req, res, next) {
  const url = `http://whoismyrepresentative.com/getall_reps_bystate.php?state=${req.params.state}&output=json`;
  request(url, handleApiResponse(res, next));
}

function findSenatorsByState(req, res, next) {
  const url = `http://whoismyrepresentative.com/getall_sens_bystate.php?state=${req.params.state}&output=json`;
  request(url, handleApiResponse(res, next));
}

function handleApiResponse(res, next) {
  return (err, response, body) => {
    if (err || body[0] === "<") {
      res.locals = {
        success: false,
        error: err || "Invalid request. Please check your state variable.",
      };
      return next();
    }
    res.locals = {
      success: true,
      results: JSON.parse(body).results,
    };
    return next();
  };
}

function jsonResponse(req, res, next) {
  return res.json(res.locals);
}

if (process.env.NODE_ENV === "production") {
  // Serve any static files
  app.use(express.static(path.join(__dirname, "client/build")));

  app.get("*", function (req, res) {
    res.sendFile(path.join(__dirname, "client/build", "index.html"));
  });
}

const server = app.listen(PORT, () => {
  const host = server.address().address,
    port = server.address().port;

  console.log("API listening at http://%s:%s", host, port);
});
