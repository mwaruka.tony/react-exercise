import React from "react";

export default function NotFound() {
  return (
    <div className="mainWrapper">
      <h1>Oops, nothing here!</h1>
    </div>
  );
}
